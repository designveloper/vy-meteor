import React from 'react';
import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {Route, BrowserRouter, Switch} from 'react-router-dom';

import App from '../imports/ui/App';
import New from '../imports/ui/New';
import Lost from '../imports/ui/Lost';

injectTapEventPlugin();

Meteor.startup(() => {
    render((
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App } />
                <Route path="/new" component={New} />
                <Route render={() => <Lost />} />
            </Switch>
        </BrowserRouter>
    ), document.getElementById('render-target'));
});

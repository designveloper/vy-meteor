import {Mongo} from 'meteor/mongo'

export const Players = new Mongo.Collection('players');

Players.allow({
    insert() {
        return false;
    },
    update() {
        return false;
    },
    remove() {
        return false;
    }
});

Players.deny({
    insert() {
        return true;
    },
    update() {
        return true;
    },
    remove() {
        return true;
    }
});

const PlayerSchema = new SimpleSchema({
    name: {type: String},
    team: {type: String},
    strength: {type: Number, defaultValue: 0},
    speed: {type: Number, defaultValue: 0},
    intelligence: {type: Number, defaultValue: 0},
    durability: {type: Number, defaultValue: 0},
    willPower: {type: Number, defaultValue: 0},
    magicalPower: {type: Number, defaultValue: 0},
    weaponHandling: {type: Number, defaultValue: 0},
    recoveryPower: {type: Number, defaultValue: 0},
    notes: {type: String, optional: true},
    owner: {type: String},
});

Players.attachSchema(PlayerSchema);

import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton'
import FontIcon from 'material-ui/FontIcon'

const selectIndex = {
    strength: 0,
    speed: 1,
    intelligence: 2,
    durability: 3,
    willPower: 4,
    magicalPower: 5,
    weaponHandling: 6,
    recoveryPower: 7,
};

const style = {
    btn: {
        backgroundColor: '#2980b9',
        width: 120,
        height: 40,
    },
    font: {
        fontSize: '16px',
        fontWeight: '600',
        color: '#ecf0f1',
        textAlign: 'center'
    }
};

export default class Edit extends Component {
    constructor(props){
        super(props);
        let currentPlayer = this.props.currentPlayer;
        this.state = {
            name: currentPlayer.name,
            team: currentPlayer.team,
            selected: [
                currentPlayer.strength,
                currentPlayer.speed,
                currentPlayer.intelligence,
                currentPlayer.durability,
                currentPlayer.willPower,
                currentPlayer.magicalPower,
                currentPlayer.weaponHandling,
                currentPlayer.recoveryPower,
            ],
            notes: currentPlayer.notes,
        };
        this.handleChange = this.handleChange.bind(this);
        this.editPlayer = this.editPlayer.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    showTeamStats() {
        this.props.showTeamStats();
    }

    handleTextChange = (event, id) => {
        this.setState({[id]: event.target.value});
    }

    handleChange = (index, event, value) => {
        var newState = this.state;
        newState.selected[index] = value;
        this.setState(newState);
    }

    editPlayer(event) {
        event.preventDefault();
        let tmpPlayer = {
            _id: this.props.currentPlayer._id,
            name: this.state.name,
            team: this.state.team,
            strength: this.state.selected[selectIndex.strength],
            speed: this.state.selected[selectIndex.speed],
            intelligence: this.state.selected[selectIndex.intelligence],
            durability: this.state.selected[selectIndex.durability],
            willPower: this.state.selected[selectIndex.willPower],
            magicalPower: this.state.selected[selectIndex.magicalPower],
            weaponHandling: this.state.selected[selectIndex.weaponHandling],
            recoveryPower: this.state.selected[selectIndex.recoveryPower],
            notes: this.state.notes,
            createdAt: new Date(),
            owner: Meteor.userId(),
        }

        Meteor.call('updatePlayer', tmpPlayer, (err) => {
            if(err) {
                alert("Something went wrong: " +  err.reason);
            } else {
                alert("Magical girl updated");
                this.showTeamStats();
            }
        });
    }

    render() {
        const currentPlayer = this.props.currentPlayer;
        return(
            <MuiThemeProvider>
                <div className="row">
                    <form className="col s12" onSubmit={this.editPlayer}>
                        <h3>Edit Magical ✰ Girl</h3>

                        <div className="row">
                            <div className="input-field col s6">
                                <h5>Name</h5>
                                <input
                                    placeholder="Your Magical Girl name"
                                    type="text"
                                    className="validate"
                                    onChange={(event, id) =>
                                         this.handleTextChange(event, "name")}
                                    defaultValue={currentPlayer.name}
                                />
                            </div>
                            <div className="input-field col s6">
                                <h5>Team</h5>
                                <input
                                    placeholder="Your Magical Girl's team" ref="team" type="text" className="validate"
                                    onChange={(event, id) =>
                                     this.handleTextChange(event, "team")}
                                    defaultValue={currentPlayer.team}
                                 />
                            </div>
                        </div>

                        <div className="row">
                            <div className="input-field col s6">
                                <h5>Strength</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.strength]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.strength, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - Strong enough to destroy a building" />
                                    <MenuItem value={2}
                                        primaryText="2 - Strong enough to destroy a city" />
                                    <MenuItem value={3}
                                        primaryText="3 - Strong enough to destroy a country" />
                                    <MenuItem value={4}
                                        primaryText="4 - Strong enough to destroy a planet/star" />
                                    <MenuItem value={5}
                                        primaryText="5 - Universe level strength" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Speed</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.speed]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.speed, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - Athletic human" />
                                    <MenuItem value={2}
                                        primaryText="2 - Superhuman" />
                                    <MenuItem value={3}
                                        primaryText="3 - Subsonic" />
                                    <MenuItem value={4}
                                        primaryText="4 - Hypersonic" />
                                    <MenuItem value={5}
                                        primaryText="5 - Infinite speed/Immeasurable/Irrelevant" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Intelligence</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.intelligence]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.intelligence, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Below average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - Normal human" />
                                    <MenuItem value={2}
                                        primaryText="2 - Gifted human" />
                                    <MenuItem value={3}
                                        primaryText="3 - Genius" />
                                    <MenuItem value={4}
                                        primaryText="4 - Nigh-Omniscience" />
                                    <MenuItem value={5}
                                        primaryText="5 - Omniscience" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Durability</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.durability]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.durability, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - City level" />
                                    <MenuItem value={2}
                                        primaryText="2 - Country level" />
                                    <MenuItem value={3}
                                        primaryText="3 - Planet level" />
                                    <MenuItem value={4}
                                        primaryText="4 - Universe level" />
                                    <MenuItem value={5}
                                        primaryText="5 - Multiverse level" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Will Power</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.willPower]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.willPower, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Below average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - Average human" />
                                    <MenuItem value={2}
                                        primaryText="2 - Knight of Justice" />
                                    <MenuItem value={3}
                                        primaryText="3 - Brave Warrior" />
                                    <MenuItem value={4}
                                        primaryText="4 - Loyal Friend" />
                                    <MenuItem value={5}
                                        primaryText="5 - Emblem of Hope/Goddess" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Magical Power</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.magicalPower]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.magicalPower, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - City level" />
                                    <MenuItem value={2}
                                        primaryText="2 - Country level" />
                                    <MenuItem value={3}
                                        primaryText="3 - Planet level" />
                                    <MenuItem value={4}
                                        primaryText="4 - Universe level" />
                                    <MenuItem value={5}
                                        primaryText="5 - Multiverse level" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Weapon Handling</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.weaponHandling]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.weaponHandling, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - Training warrior" />
                                    <MenuItem value={2}
                                        primaryText="2 - Average warrior" />
                                    <MenuItem value={3}
                                        primaryText="3 - Skillful warrior" />
                                    <MenuItem value={4}
                                        primaryText="4 - Weapon Master" />
                                    <MenuItem value={5}
                                        primaryText="5 - Ultimate Weapon Master" />
                                </SelectField>
                            </div>

                            <div className="input-field col s6">
                                <h5>Recovery Power</h5>
                                <SelectField
                                    value={this.state.selected[selectIndex.recoveryPower]}
                                    onChange={(index, event, value) =>
                                        this.handleChange(selectIndex.recoveryPower, event, value)}
                                    fullWidth={true}
                                    autoWidth={true}
                                    >
                                    <MenuItem value={0}
                                        primaryText="0 - Below average human" />
                                    <MenuItem value={1}
                                        primaryText="1 - Average human" />
                                    <MenuItem value={2}
                                        primaryText="2 - Accelerate recovery" />
                                    <MenuItem value={3}
                                        primaryText="3 - Minor injures healed immediately" />
                                    <MenuItem value={4}
                                        primaryText="4 - Heavy injures healed immediately" />
                                    <MenuItem value={5}
                                        primaryText="5 - Absolutely immediate recovery" />
                                </SelectField>
                            </div>

                        </div>

                        <div className="row">
                            <div className="input-field col s6">
                                <textarea placeholder="Notes"  className="materialize-textare"
                                onChange={(event, id) =>
                                     this.handleTextChange(event, "name")}
                                defaultValue={currentPlayer.notes}
                                />
                            </div>
                            <div className="input-field col s6">
                                <RaisedButton
                                    label="Save"
                                    labelPosition="before"
                                    icon={<FontIcon className="material-icons md-18">
                                            send</FontIcon>}
                                    overlayStyle={style.btn}
                                    labelStyle={style.font}
                                    type="submit"
                                    name="action">
                                </RaisedButton>
                            </div>
                        </div>
                    </form>
                </div>
            </MuiThemeProvider>
        )
    }
}

// export default withRouter(New);

import React, {Component} from 'react';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';

const styles = {
    chip: {
        margin: 4,
        backgroundColor: "#c9d787",
        color: "#2c3e50",
    },
    ava: {
        backgroundColor: "#ff8598",
        color: "#2c3e50",
    }
};

export default class SkillChip extends Component {
    render() {
        return(

            <Chip
                style={styles.chip}
            >
            <Avatar size={32}
                style={styles.ava}
            >
                {this.props.data}
            </Avatar>
                {this.props.title}
            </Chip>

        )
    }
}

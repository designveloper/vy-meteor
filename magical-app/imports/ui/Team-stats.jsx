import React, {Component} from 'react';
import {Radar} from 'react-chartjs-2';
import Divider from 'material-ui/Divider'

export default class TeamStats extends Component {
    render() {
        const players = this.props.players;
        const nGirls = players.length;

        const strength = Math.round((players.reduce((strength, player) => {
            return strength + player.strength;
        }, 0) / (6 * nGirls)) * 100);

        const speed = Math.round((players.reduce((speed, player) => {
            return speed + player.speed;
        }, 0) / (6 * nGirls)) * 100);

        const intelligence = Math.round((players.reduce((intelligence, player) => {
            return intelligence + player.intelligence;
        }, 0) / (6 * nGirls)) * 100);

        const durability = Math.round((players.reduce((durability, player) => {
            return durability + player.durability;
        }, 0) / (6 * nGirls)) * 100);

        const willPower = Math.round((players.reduce((willPower, player) => {
            return willPower + player.willPower;
        }, 0) / (6 * nGirls)) * 100);

        const magicalPower = Math.round((players.reduce((magicalPower, player) => {
            return magicalPower + player.magicalPower;
        }, 0) / (6 * nGirls)) * 100);

        const weaponHandling = Math.round((players.reduce((weaponHandling, player) => {
            return weaponHandling + player.weaponHandling;
        }, 0) / (6 * nGirls)) * 100);

        const recoveryPower = Math.round((players.reduce((recoveryPower, player) => {
            return recoveryPower + player.recoveryPower;
        }, 0) / (6 * nGirls)) * 100);

        const attack = Math.round(((strength + magicalPower*2)*100 + (speed + weaponHandling)*50 + weaponHandling*25));
        const defense = Math.round(((durability*3 + recoveryPower)*100 + (speed + willPower)*50 + (intelligence + magicalPower)*25));

        const data = {
          labels: ['Strength', 'Speed', 'Intelligence', 'Durability', 'Will Power', 'Magical Power', 'Weapon Handling', 'Recovery Power'],
          datasets: [
            {
              label: 'In % of max possible',
              backgroundColor: 'rgba(255, 192, 169, 0.5)',
              borderColor: 'rgba(255, 133, 152, 0.7)',
              pointBackgroundColor: 'rgba(125, 138, 46, 0.5)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(179,181,198,1)',
              data: [strength, speed, intelligence, durability, willPower, magicalPower, weaponHandling, recoveryPower]
            },
          ]
        };

        return(
            <div>
                <h2>Team Stats</h2>
                <div className="row">
                    <div className="col s12 m7">
                        <Radar data={data}
                            width={500}
                            height={500}
                            option={{
                                maintainAspectRatio: false,
                            }} />
                    </div>

                    <div className="col s12 m5">
                        <h4>Scores in % of max possible</h4>
                        <Divider />
                        <h4>Team's Attack: {attack}</h4>
                        <h4>Team's Defense: {defense}</h4>
                        <Divider />
                        <h4>Number of Magical Girls: {nGirls}</h4>
                    </div>
                </div>
            </div>
        )
    }
}

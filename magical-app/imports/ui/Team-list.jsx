import React, {Component} from 'react';
import Avatar from 'material-ui/Avatar';
import {ListItem} from 'material-ui/List';
import ActionDeleteForever from 'material-ui/svg-icons/action/delete-forever';

export default class TeamList extends Component {
    updateCurrentPlayer(player) {
        this.props.updateCurrentPlayer(player);
    }

    deletePlayer(playerId) {
        Meteor.call('deletePlayer', playerId, (err) => {
            if(err) {
                alert("Something went wrong: " +  err.reason);
            } else {
                console.log("Magical Girl deleted...");
            }
        });
    }

    render() {
        return(
            <ListItem
                primaryText={this.props.player.name}
                leftAvatar={<Avatar src="player1.jpg" />}
                rightIcon={<ActionDeleteForever
                    hoverColor="#e74c3c"
                    onClick={this.deletePlayer.bind(this, this.props.player._id)} />}
                onClick={this.updateCurrentPlayer.bind(this, this.props.player)}
                 />
        )
    }
}

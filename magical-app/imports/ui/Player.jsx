import React, {Component} from 'react';
import {Card, CardMedia, CardTitle, CardText, CardActions} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import {yellow500, pink200, grey900} from 'material-ui/styles/colors';

import SkillChip from './SkillChip'

const styles = {
    chip: {
        margin: 4,
    },
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    button: {
        margin: 12,
    },
};

export default class Player extends Component {
    showEditForm() {
        this.props.showEditForm();
    }

    render() {
        const player = this.props.player;
        const atk = (player.strength + player.magicalPower*2)*100 + (player.speed + player.weaponHandling)*50 + player.weaponHandling*25;
        const def = (player.durability*3 + player.recoveryPower)*100 + (player.speed + player.willPower)*50 + (player.intelligence + player.magicalPower)*25;

        return(
            <Card>
                <CardMedia
                  overlay={<CardTitle title={player.name} subtitle={`Attack: ${atk} - Defense: ${def}`}/>}
                >
                  <img src="player1.jpg" alt="God" />
                </CardMedia>
                <CardText>
                    <div style={styles.wrapper}>
                        <SkillChip title="Strength"
                            data={player.strength} />
                        <SkillChip title="Speed"
                            data={player.speed} />
                        <SkillChip title="Intelligence"
                            data={player.intelligence} />
                        <SkillChip title="Durability"
                            data={player.durability} />
                        <SkillChip title="Will Power"
                            data={player.willPower} />
                        <SkillChip title="Magical Power"
                            data={player.magicalPower} />
                        <SkillChip title="Weapon Handling"
                            data={player.weaponHandling} />
                        <SkillChip title="Recovery Power"
                            data={player.recoveryPower} />
                    </div>
                </CardText>
                <CardActions>
                    <RaisedButton
                        label="Edit Magical Girl"
                        labelPosition="before"
                        style={styles.button}
                        onClick={this.showEditForm.bind(this)}
                    />
                </CardActions>
              </Card>
        )
    }
}

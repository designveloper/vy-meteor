import React, {Component, PropTypes} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import {List} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import {createContainer} from 'meteor/react-meteor-data';
import {Link} from 'react-router-dom';

//database collection
import {Players} from '../api/players'

import Player from './Player'
import TeamList from './Team-list'
import TeamStats from './Team-stats'
import AccountsWrapper from './AccountsWrapper';
import Edit from './EditPlayer'

const tmpGirl = {
    name: "Temp character",
    team: "Holy Quintet",
    strength: 2,
    speed: 2,
    intelligence: 1,
    durability: 2,
    willPower: 3,
    magicalPower: 3,
    weaponHandling: 2,
    recoveryPower: 2,
    notes: "Temporary Charater ONLY",
}

export class App extends Component {
    constructor(props) {
        super(props);
        //setting up the state
        this.state = {
            currentPlayer: tmpGirl,
            showEditPlayer: false,
        };
        this.updateCurrentPlayer = this.updateCurrentPlayer.bind(this);
        this.showEditForm = this.showEditForm.bind(this);
        this.showTeamStats = this.showTeamStats.bind(this);
    }

    renderPlayers() {
        return this.props.players.map((player) => (
            <TeamList key={player._id} player={player}
                updateCurrentPlayer={this.updateCurrentPlayer}
             />
        ));
    }

    updateCurrentPlayer(player) {
        this.setState({
            currentPlayer: player,
        });
    }

    showEditForm() {
        this.setState({
            showEditPlayer: true,
        });
    }

    showTeamStats() {
        this.setState({
            showEditPlayer: false,
        });
    }

    showForm() {
        if(this.state.showEditPlayer === true){
            return( <Edit currentPlayer={this.state.currentPlayer}
                showTeamStats={this.showTeamStats}/> );
        } else {

            return (<TeamStats players={this.props.players}/>);
        }
    }

    render() {
        return(
            <MuiThemeProvider>
                {/* col is column s12, standard grid has 12 cols
                number tells how many columns wide the div takes
                s, m, l, xl is screen size (for responsive purpose)
                normal we use s12 > will be understand as s12 m12 l12 */}
                <div className="container">
                    <AppBar
                        title="Magical✰Girl Application"
                        iconClassNameRight="muidocs-icon-navigation-expand-more"
                        showMenuIconButton={false}
                        style={{backgroundColor:'#ff8598'}}
                        >
                        <AccountsWrapper />
                    </AppBar>
                    <div className="row">
                        <div className="col s12 m7">
                            <Player player={this.state.currentPlayer}
                                showEditForm={this.showEditForm}
                            />
                        </div>
                        <div className="col s12 m5">
                            <h2>Team List</h2>
                            <Link to="/new" className="btn waves-effect waves-light" style={{backgroundColor:'#c9d787'}}>
                                Add New Magical Girl
                            </Link>
                            <Divider />
                                <List>
                                    {this.renderPlayers()}
                                </List>
                            <Divider />
                        </div>

                    </div>
                    <div className="row">
                        <div className="col s12">
                            <br/>
                            <Divider />
                            {this.showForm()}
                            <Divider />
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        )
    }
}

App.propTypes = {
    players: PropTypes.array.isRequired,
};

export default createContainer(() => {
    Meteor.subscribe('players');
    const user = Meteor.userId();

    return {
        players: Players.find({owner: user}, {sort: {name: 1}}).fetch(),
    };
}, App);

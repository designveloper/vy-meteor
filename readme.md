# DSV - Meteor

## Setting up
* Create new meteor pj: `meteor create [name]`
* Run meteor pj on server: `meteor` (in the root dir)

## React Refresher:
* stateful vs. stateless: smart vs. dumb
    * stateless: not do much on its own. Need a parent to do something
    * stateful: do their job, on their own. Where the logic and state resides
* props: used to pass data from state to stateless component, from the parent to children
* [Google Material Icons](http://google.github.io/material-design-icons/)
* [Materialize-CSS](http://materializecss.com/getting-started.html)
* [Material-UI](http://www.material-ui.com/#/)
* [Flexbox](https://www.w3schools.com/css/css3_flexbox.asp): a new layout mode in CSS3. Flexibility. An improvement for block model, for float. [Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/). With demonstration (VNmese) [as gif](http://ntuts.com/huong-dan/cach-thuc-hoat-dong-cua-flexbox) and [as image](https://reactvn.org/t/tim-hieu-ve-css-flexbox/48)
* in any React component, there's only 1 `<div>`
* the `ref` keyword is basically a reference to an obj, it can be used to match with the database key

## Meteor
* Meteor [packages](https://www.lynda.com/React-js-tutorials/Meteor-introduction/533228/572378-4.html)
* [Yarn](https://code.facebook.com/posts/1840075619545360) - the Fb package manager (not work well on wnd)
* `path="*"` is good for doing an error/404 not found page
* **Routing**: `react-router`, `browserHistory`, `Router`,... are out-of-date => use `react-router-dom` with `BrowserRouter` instead (child elements wrapped in `<Switch>` tag - using the `exact` keyword for absolute path)/ or using `hashHistory` instead of `browserHistory`
* Mongo doesnt recognize `.jsx` format
* **Schema**: a way to validate the type of data sending to the database is of the type we supposed to get.
    * import `{Mocha}` from `meteor/mongo`
    * create new Mongo db: `new Mongo.Collection('name')`
    * create new schema: `new SimpleSchema`
    * regulating the data type: `keyword: {type: String/Number/..}`
        * we can define a default value in the `{type}`, for case we dont select the instance, and the db would still not return error
        * we can specify an item as `optional`
    * attach to database: `[dbName].attachSchema([schemaName])`
* Materialize [input-field](http://materializecss.com/forms.html)
* Materialize [waves](http://materializecss.com/waves.html#!)
* Refer to event obj in `onChange` event: [stackoverflow](https://stackoverflow.com/questions/37112712/react-onchange-event-doesnt-return-object)
* `browserHistory.push()` => `this.props.history.push()`
    and `export default withRouter([component])`
* Packages support user login in meteor: [User Accounts](https://github.com/meteor-useraccounts/core/blob/master/Guide.md#available-versions)
    * return the user ID: `Meteor.userId()`
* `publish` and `subscribe` approach are used to control the data snt to the client from the server
* Securing:
    * use `Meteor.call` to call a Meteor function
    * doing db insertion on server and using a `Meteor.call` method to initiate from the client
* [Chart](http://www.chartjs.org)
